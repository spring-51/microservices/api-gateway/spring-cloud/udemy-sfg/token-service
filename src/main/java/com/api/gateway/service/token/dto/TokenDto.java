package com.api.gateway.service.token.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor @Builder
@ToString
public class TokenDto {
    @Builder.Default
    private UUID uuid = UUID.randomUUID();
    private String token;
}
