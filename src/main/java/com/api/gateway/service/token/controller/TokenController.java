package com.api.gateway.service.token.controller;

import com.api.gateway.service.token.dto.TokenDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/tokens")
public class TokenController {

    @Value(value = "${spring.application.name}")
    private String applicationName;

    @GetMapping
    public ResponseEntity<?> getAll(){
        List<TokenDto> res = new ArrayList<>();
        res.add(TokenDto.builder().token(UUID.randomUUID().toString()+"##1").build());
        res.add(TokenDto.builder().token(UUID.randomUUID().toString()+"##2").build());
        res.add(TokenDto.builder().token(UUID.randomUUID().toString()+"##3").build());
        System.out.println("************" + applicationName + "********************");
        System.out.println(">>>>>>>>>>>> TokenController -> getAll <<<<<<<<<<<<<<<<");
        System.out.println("############" + res + "####################");
        return ResponseEntity.ok(res);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable String id){
        TokenDto res = TokenDto.builder().token(id).build();
        System.out.println("************" + applicationName + "********************");
        System.out.println(">>>>>>>>>>>> TokenController -> getById <<<<<<<<<<<<<<<<");
        System.out.println("############" + res + "####################");
        return ResponseEntity.ok(res);
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody TokenDto request){
        System.out.println("************" + applicationName + "********************");
        System.out.println(">>>>>>>>>>>> TokenController -> post <<<<<<<<<<<<<<<<");
        System.out.println("############" + request + "####################");
        return ResponseEntity.ok(request);
    }
}
